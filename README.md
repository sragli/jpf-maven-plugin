# README #

jpf-maven-plugin is a Maven plugin for [Java Pathfinder](http://babelfish.arc.nasa.gov/trac/jpf), a software verification tool for Java applications.
It provides runtime error checking using the verification methods implemented by *jpf-aprop* and *jpf-numeric*.
Using this plugin, there is no need to couple the unit test cases with JPF, conventional JUnit tests can be
run on the JPF VM without any modification.

### Requirements ###

SUT (System Under Test) should be a Java application consisting of classes compiled to Java version <= 1.6
Maven-based project build. 

### How To Use The Plugin ###

* Edit your pom.xml. Add the jpf-maven-plugin to the "plugins" section:

```
#!xml

<plugin>
        <groupId>hu.ngms.jpf</groupId>
        <artifactId>jpf-maven-plugin</artifactId>
        <version>1.2</version>
        <executions>
                <execution>
                        <phase>test</phase>
                        <goals>
                                <goal>jpftest</goal>
                         </goals>
                </execution>
        </executions>
</plugin>
```

* Add the following dependencies to the dependencies section:

```
#!xml

<dependency>
        <groupId>hu.ngms.jpf</groupId>
        <artifactId>jpf-maven-plugin</artifactId>
        <version>1.2</version>
</dependency>
<dependency>
        <groupId>gov.nasa.jpf</groupId>
        <artifactId>jpf-aprop-annotations</artifactId>
        <version>[58,)</version>
</dependency>
```

* Add the Maven repository and plugin repository to the repositories and the pluginRepositories section:

```
#!xml

<repositories>
        <repository>
                <id>jpf</id>
                <url>http://maven.ngms.hu/nexus/content/repositories/jpf/</url>
        </repository>
</repositories>

<pluginRepositories>
        <pluginRepository>
                <id>jpf</id>
                <url>http://maven.ngms.hu/nexus/content/repositories/jpf/</url>
        </pluginRepository>
</pluginRepositories>
```

* Create a file named jpf.properties in the project home with the following content:

```
#!java

your_project_name = ${config_path}
+your_project_name.classpath = target/classes; target/test-classes
+your_project_name.test_classpath = target/test-classes
```

Replace your_project_name with the name of your Maven project. 

For more details on how to set up a Maven project to use this plugin, check out the [jpf-maven-test](https://bitbucket.org/sragli/jpf-maven-test) sample project.