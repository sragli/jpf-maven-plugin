package hu.ngms.jpf.maven;


import static org.junit.Assert.*;
import hu.ngms.jpf.maven.JUnitWrapper;

import org.junit.Before;
import org.junit.Test;

public class JUnitWrapperTest {

	private JUnitWrapper wrapper;
	
	@Before
	public void setUp() throws Exception {
		wrapper = new JUnitWrapper();
	}

	@Test
	public void testDelegation() {
		wrapper.delegateTestRunning(InputTest.class.getName());
	}
	
	public static class InputTest {
		
		@Test
		public void testSuccess() {
			assertTrue(true);
		}
		
		@Test(expected=AssertionError.class)
		public void testFailure() {
			fail();
		}
		
		
	}

}
