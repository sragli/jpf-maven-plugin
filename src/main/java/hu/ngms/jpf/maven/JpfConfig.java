package hu.ngms.jpf.maven;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class JpfConfig {

	private static final String[] CONFIG = {
			"+vm.insn_factory.class=.numeric.bytecode.InstructionFactory",
			"+listener+=,.aprop.listener.ConstChecker",
			"+listener+=,.aprop.listener.ContractVerifier",
			"+listener+=,.aprop.listener.NonnullChecker",
			"+listener+=,.aprop.listener.SandBoxChecker",
			"+listener+=,.listener.PreciseRaceDetector",
			"+listener+=,.listener.DeadlockAnalyzer",
			JUnitWrapper.class.getName()
	};
	
	private List<String> jpfConfig = new ArrayList<String>();
	
	public JpfConfig() {
		jpfConfig.addAll(Arrays.asList(CONFIG));
	}
	
	public void setTestClasses(List<String> testClasses) {
		jpfConfig.addAll(testClasses);
	}
	
	public String[] toArray() {
		return jpfConfig.toArray(new String[jpfConfig.size()]);
	}

}
