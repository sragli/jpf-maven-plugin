package hu.ngms.jpf.maven;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

public class SymbcWrapper {
	
	public void runSymbcTests(String... sutClasses) {
		try {
			for (String className : sutClasses) {
				Class<?> clazz = Class.forName(className);
				List<String> methodSignatures = symbcMethodSignaturesOf(clazz);
				// TODO run symbc test for these methods and store the output test vectors
			}
		} catch (ClassNotFoundException e) {
			throw new RuntimeException(e);
		}
	}
	
	private List<String> symbcMethodSignaturesOf(Class<?> clazz) {
		List<String> methodSignatures = new ArrayList<String>();
		
		Method[] methods = clazz.getMethods();
		for (Method method : methods) {
			if (method.isAccessible()) {
				StringBuilder signature = new StringBuilder(method.getName() + "(");
				
				Class<?>[] parameters = method.getParameterTypes();
				for (int i = 0; i < parameters.length - 1; i++) {
					signature.append(symbcMethodParameterFrom(parameters[i])).append("#");
				}
				signature.append(symbcMethodParameterFrom(parameters[parameters.length - 1]));

				signature.append(")");
				
				methodSignatures.add(signature.toString());
			}
		}
				
		return methodSignatures;
	}
	
	private String symbcMethodParameterFrom(Class<?> parameter) {
		String result = null;
		
		if (parameter.isPrimitive()) {
			result = "con";
		} else {
			result = "sym";
		}
		
		return result;
	}

}
